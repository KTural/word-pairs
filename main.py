#!/usr/bin/env python3

import sys, os, argparse, requests
from collections import Counter
from flask import Flask, request, jsonify
from bs4 import BeautifulSoup

app = Flask("Word pairs")

WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/"

# Split text into word pairs
def find_top_word_pairs(text, n=10):
    # get word pairs and count their occurences
    words = text.split()
    pair_counts = Counter([(words[i], words[i + 1]) for i in range(len(words) - 1)])
    # get the top 10 word pairs
    top_pairs = pair_counts.most_common(n)
    return top_pairs

@app.route("/word-pairs", methods=["POST"])
def analyze_file():
    try:
        # Access the uploaded file from the request
        uploaded_file = request.files["file"]
        # Read the contents of the uploaded file
        file_contents = uploaded_file.read().decode("utf-8")
        # Call the function to find the top word pairs
        top_pairs = find_top_word_pairs(file_contents)
        return jsonify({"Most frequent pairs": top_pairs})
    except Exception as e:
        return jsonify({"Error": str(e)})

@app.route("/wikipedia-content", methods=["GET"])
def get_wikipedia_content():
    try:
        # Get the name of the Wikipedia article from the query parameters
        article_name = request.args.get("article_name")
        if not article_name:
            return jsonify({"Error": "Please provide the 'article_name' parameter"}), 400
        # Construct the URL for the Wikipedia article
        url = WIKIPEDIA_URL + article_name
        # Send a GET request to fetch the HTML content of the Wikipedia article
        response = requests.get(url)
        if response.status_code == 200:
            # Parse the HTML content using BeautifulSoup
            soup = BeautifulSoup(response.text, "html.parser")
            # Find and extract the main content of the article
            main_content = soup.find("main").get_text().encode("utf-8").decode("utf-8")
            if main_content:
                # Strip and clean blank lines from the content
                cleaned_content = " ".join([line.strip() for line in main_content.split() if len(line) != 0])
                # Call the function to find the top word pairs
                top_pairs = find_top_word_pairs(cleaned_content)
                return jsonify({"Most frequent pairs": top_pairs})
            else:
                return jsonify({"Error": "Article content not found"}), 404
        else:
            return jsonify({"Error": "Failed to fetch Wikipedia article"}), 500
    except Exception as e:
        return jsonify({"Errors": str(e)}), 500

if __name__ == "__main__":
    # parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--web", action="store_true", help="Start the web server")
    parser.add_argument("--file", help="Input file path")
    args = parser.parse_args()
    
    if args.web:
        app.run(host="0.0.0.0", port=5000, debug=True)
    else:
        # check whether file exists
        if os.path.exists(args.file):
            with open(args.file, 'r') as file:
                text = file.read()
            # Print the top 10 most frequent word pairs
            for pair, count in find_top_word_pairs(text):
                print(f"Pair: ({pair[0]}, {pair[1]}) - Count: {count}")
        else:
            sys.exit(f"{args.file} doesn't exist!")
