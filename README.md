# **Word Pairs** #

### GOALS
Write a Python script that reads an input plaintext file and prints out the top 10 most frequently occurring pairs of successive words (words that follow immediately after each other).Create a Dockerfile that encapsulates this script along with any required dependencies. Run the script as a web API that accepts the file in a POST request.
Add another GET endpoint that accepts the name of a wikipedia article and uses that as input.


## Installation

 Install Docker : https://docs.docker.com/engine/install/

```sh
# Check whether python installed:
$ python --version
# Check whether pip installed:
$ python -m pip --version
# If above all installed:
$ pip install -r requirements.txt
```

## Usage/Examples

See usage of the program:
```sh
$ ./main.py --help
usage: main.py [-h] [--web] [--file FILE]

options:
  -h, --help   show this help message and exit
  --web        Start the web server
  --file FILE  Input file path
```

Run the program with sample input file (word pairs are in descending order):
```sh
$ ./main.py --file samplefile.txt
Pair: (sit, amet) - Count: 7
Pair: (Aliquam, erat) - Count: 6
Pair: (erat, volutpat.) - Count: 6
Pair: (in, pharetra) - Count: 6
Pair: (dolor, sit) - Count: 5
Pair: (sit, amet,) - Count: 5
Pair: (diam, non) - Count: 4
Pair: (Suspendisse, sagittis) - Count: 3
Pair: (sagittis, ultrices) - Count: 3
Pair: (ultrices, augue.) - Count: 3
```
Run the program as a web api:

* Start the server
  ```sh
  $ ./main.py --web
  * Serving Flask app 'Word pairs'
  * Debug mode: on 
  * Running on all addresses (0.0.0.0)
  * Running on http://127.0.0.1:5000
  * Running on http://10.128.58.81:5000
  Press CTRL+C to quit
  * Restarting with stat
  * Debugger is active!
  * Debugger PIN: 179-461-832 
  ```
* Send the POST request (on another session) to the server that contains the sample input file 
  ```sh
  $ curl -X POST -F "file=@./samplefile.txt" http://127.0.0.1:5000/word-pairs
  {
  "Most frequent pairs": [
      [
        [
          "sit",
          "amet"
        ],
        7
      ],
      [
        [
          "Aliquam",
          "erat"
        ],
        6
      ],
      [
        [
          "erat",
          "volutpat."
        ],
        6
      ],
      [
        [
          "in",
          "pharetra"
        ],
        6
      ],
      [
        [
          "dolor",
          "sit"
        ],
        5
      ],
      [
        [
          "sit",
          "amet,"
        ],
        5
      ],
      [
        [
          "diam",
          "non"
        ],
        4
      ],
      [
        [
          "Suspendisse",
          "sagittis"
        ],
        3
      ],
      [
        [
          "sagittis",
          "ultrices"
        ],
        3
      ],
      [
        [
          "ultrices",
            "augue."
        ],  
        3
      ]
    ]
  }
  ```
* Send GET request (on another session) that accepts wikipedia article name as query
  ```sh
  $ curl -X GET http://127.0.0.1:5000/wikipedia-content\?article_name\=Lorem_ipsum
  {
    "Most frequent pairs": [
      [
        [
          "Lorem",
          "ipsum"
        ],
        19
      ],
      [
        [
          "of",
          "the"
        ],
        12
      ],
      [
        [
          "used",
          "in"
        ],
        6
      ],
      [
        [
          "form",
          "of"
        ],
        4
      ],
      [
        [
          "De",
          "finibus"
        ],
        4
      ],
      [
        [
          "finibus",
          "bonorum"
        ],
        4
      ],
      [
        [
          "bonorum",
          "et"
        ],
        4
      ],
      [
        [
          "in",
          "the"
        ],
        4
      ],
      [
        [
          "in",
          "a"
        ],
        3
      ],
      [
        [
          "to",
          "the"
        ],
        3
      ]
    ]
  }
  ```


## Deployment

  **Docker Hub (repository) image:**
    https://hub.docker.com/r/turalkazimli/word-pairs

  * Build the docker image:

    ```sh
    $ docker build -t word-pairs .
    ```

  * Run the docker container in an interactive mode:

    ```sh
    $ docker run -it --rm word-pairs /bin/bash
    ```

  * Determine IP address of your running container:
    ```sh
    $ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container_id>

    ```
    * To get the container id:
      ```sh
      $ docker ps
      ```

  * Directly run the docker container:
    ```sh
    $ docker run word-pairs
    ```

  * Pull the image from Docker Hub and run locally:
    ```sh
    $ docker pull turalkazimli/word-pairs:latest
    $ docker run turalkazimli/word-pairs:latest
    ```


## References
- https://flask.palletsprojects.com/en/2.3.x/#user-s-guide
- https://docs.docker.com/engine/reference/commandline/cli/
- https://beautiful-soup-4.readthedocs.io/en/latest/#
- https://docs.python.org/3/library/argparse.html
- https://docs.python-requests.org/en/latest/index.html


