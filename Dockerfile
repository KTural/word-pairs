FROM python:3.8 as builder
WORKDIR /project
COPY . /project
RUN pip install --no-cache-dir --upgrade -r /project/requirements.txt

EXPOSE 80
CMD ["python3", "main.py", "--web"]
